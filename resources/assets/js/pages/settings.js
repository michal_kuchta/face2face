var VueResource = require('vue-resource');
var Vue = require('vue');
var Notifications = require('vue-notification');
 
Vue.use(Notifications);
Vue.use(VueResource);
window.addEventListener('load', function () {
    var vm = new Vue({
        el: '#content',
        data: {
            notifications: [],
            showMorePosts: false,
            friends: []
        },
        mounted: function(){
            
        },
        methods: {
            setNotification: function(type, title, content)
            {
                let notification = {
                    type: type,
                    title: title,
                    content: content
                };
                let index = this.notifications.push(notification) - 1;
                setTimeout(() => this.removeNotification(index), 3000);
            },
            removeNotification: function(index)
            {
                this.notifications.splice(index, 1);
            }
        }
    })
})