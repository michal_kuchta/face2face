<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('partials.head')
    <body>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="success">
                <ul>
                    @foreach (session('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('partials.mainTopBar')
        <div class="content" id="content">
            <div class="mainContent">
                @yield('content')
            </div>
            @include('partials.notifications')
        </div>
        
    @yield('scriptsFooter')
    <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
