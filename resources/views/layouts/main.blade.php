<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('partials.head')
    <body>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="success">
                <ul>
                    @foreach (session('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('partials.mainTopBar')
        <div class="content">
            <div class="mainContent">
                <div class='row'>
                    <div class="col-7" id="content" v-cloak>
                        @yield('content')
                        @include('partials.notifications')
                    </div>
                    <div class="col" id="friends" v-cloak>
                        @include('partials.friendsSection')
                    </div>
                </div>
                
            </div>
        </div>
        
    @yield('scriptsFooter')
    <script type="text/javascript" src='/js/pages/friends.js'></script>
    <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
