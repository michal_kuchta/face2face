@component('mail::message')
{{__('Witaj')}} {{$user->first_name}}!

{{__('Twoje konto zostało aktywowane pomyślnie. Od teraz możesz używać swojego konta bez przeszkód!')}}

@component('mail::button', ['url' => url()->route('login')])
{{__('Zaloguj się')}}
@endcomponent
@endcomponent