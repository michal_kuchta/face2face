<div class='row' v-for='friend in friendsList'>
    <div class='col mainPhoto'>
        <a v-bind:href='friend.profile_link'><img v-bind:src='friend.image' class=''></a>
    </div>
    <div class='col name-col'>
        <a v-bind:href='friend.profile_link'>@{{friend.name}}</a>
    </div>
</div>