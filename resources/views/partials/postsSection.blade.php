<div class="addPostForm row" v-if='displayPostForm === true'>
    <form action='' >
        <div class="row">
            <div class="col">
                <h3>{{__('Utwórz post')}}</h3>
            </div>
        </div>
        <div class='row'>
            <div class="col">
                <div class="form-group">
                    <textarea name="post_message" class="form-control" rows="5" placeholder="{{__('Co u Ciebie słychać?')}}" v-model="post.message"></textarea>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <select name="visibility" class="form-control" v-model="post.visibility">
                            <option selected="selected" value="0">{{ __('Publicznie') }}
                            <option value="1">{{ __('Tylko znajomi') }}
                            <option value="2">{{ __('Tylko ja') }}
                        </select>
                    </div>
                    <div class="col">

                    </div>
                    <div class="col" style='text-align: right;'>
                        <span class="publishPostBtn btn btn-primary" @click.stop="publishPost()">{{ __('Opublikuj') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class='row postBox' v-for="(post, index) in posts">
    <div class="post row">
        <div class="header row">
            <div class="col photo">
                <a v-bind:href="post.user.profileLink"><img class='mainPhoto' v-if="post.user.image != ''" v-bind:src="post.user.image" /></a>
            </div>
            <div class='col data'>
                <div class='row'>
                    <span><a v-bind:href="post.user.profileLink">@{{post.user.name}}</a></span>
                </div>
                <div class='row'>
                    @{{post.time}}
                </div>
            </div>
            <div class='col visibility'>
                <span v-bind:class="post.visibility.icon"></span>
            </div>
        </div>
        <div class="row message">
            @{{post.message}}
        </div>
        <div class="row action_btns">
            <span class='col'><span class='likes' v-if='post.likes > 0'>@{{post.likes}}</span><span v-bind:class='post.likeIcon' class='like_btn' @click.stop="likePost(post.id, index)"><span class="text">{{ __('Polub') }}</span></span></span>
            <span class='col commentsBtnBox'><span class='f2f-comments-o comments' @click.stop="displayComments(post.id, index)">{{__('Komentarze:')}}@{{post.ammountOfComments}}</span></span>
        </div>
    </div>
    <div class="row commentsSection" v-if="post.displayCommentsSection === true">
        <div class='row commentForm'>
            <div class="form-group">
                <textarea name="comment_message" class="form-control" rows="3" placeholder="{{__('Napisz wiadomość')}}" v-model="post.commentModel.message"></textarea>
            </div>
            <div class="row form-group">
                <div class="col">

                </div>
                <div class="col">

                </div>
                <div class="col" style='text-align: right;'>
                    <span class="publishPostBtn btn btn-primary" @click.stop="addComment(post.id, index)">{{ __('Dodaj') }}</span>
                </div>
            </div>
            <div class="row comment" v-for="(comment, index) in post.comments">
                <div class="header row">
                    <div class="col photo">
                        <img class='mainPhoto' v-if="comment.user.image != ''" v-bind:src="comment.user.image" />
                    </div>
                    <div class='col data'>
                        <div class='row'>
                            <span>@{{comment.user.name}}</span>
                        </div>
                        <div class='row'>
                            @{{comment.time}}
                        </div>
                    </div>
                </div>
                <div class="row message">
                    @{{post.message}}
                </div>
            </div>
            <div class="row loadMore" v-if="post.showMoreComments">
                <span @click.stop="loadMoreComments(post.id, index)" class="loadMoreBtn">{{__('Załaduj więcej')}}</span>
            </div>
        </div>
    </div>
</div>
<div class="row loadMore" v-if="showMorePosts">
    <span @click.stop="loadMorePosts" class="loadMoreBtn">{{__('Załaduj więcej')}}</span>
</div>

