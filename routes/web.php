<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/activate/{activation_code}/{user}', 'AuthController@GetActivate');

Route::middleware('auth')->group(function(){
    Route::get('/', 'HomeController@getIndex')->name('home');
    
    Route::get('/logout', 'AuthController@getLogout')->name('logout');
    
    Route::get('/post/{id}', function(){return 1;});
    Route::delete('/post/{id}', function(){return 1;});
    Route::get('/comment/{id}', function(){return 1;});
    Route::delete('/comment/{id}', function(){return 1;});
    
    Route::get('/settings', 'SettingsController@getIndex')->name('settings');
    Route::post('/settings', 'SettingsController@saveSettings');
    
    Route::post('/post', 'PostsController@createPost');
    Route::post('/comment', 'PostsController@addComment');
    Route::post('/post/like/{id}', 'PostsController@likePost');
    Route::get('/posts/{user_id}/{limit}/{offset}', 'PostsController@getPosts');
    Route::get('/comments/{post_id}/{limit}/{offset}', 'PostsController@getCommentsForPost');
    
    Route::get('/relation/getList', 'UserProfileController@getFriendList');
    
    Route::post('/relation/user/{action}/{user_id}', 'UserProfileController@manageFriends');
    Route::get('/relation/user/getUsersFriendsList/{user_id}', 'UserProfileController@getUsersFriendsList');
    Route::get('/profile/{id}', 'UserProfileController@getIndex')->name('profile');
});

Route::middleware('guest')->group(function(){
    Route::get('/login', 'AuthController@getLogin')->name('login');
    Route::post('/login', 'AuthController@postLogin');
    Route::get('/remind', 'AuthController@getRemind')->name('remind');
    Route::post('/remind', 'AuthController@postRemind');
    Route::post('/register', 'AuthController@postRegister');
});
