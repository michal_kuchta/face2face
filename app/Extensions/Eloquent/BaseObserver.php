<?php namespace app\Extensions\Eloquent;

/**
 * Class BaseObserver
 *
 * @package app\Extensions\Eloquent
 */
abstract class BaseObserver
{
    /** @var array */
    protected static $disabled = [];

    /**
     * @param object $model
     * @param callable $callback
     */
    public static function dismiss($model, callable $callback)
    {
        $observerName = static::class.':'.get_class($model);
        self::$disabled[$observerName] = true;
        call_user_func($callback, $model);
        self::$disabled[$observerName] = false;
    }
    
    /**
     * @param string $observerName
     * @param object $model
     */
    protected function enable($observerName, $model)
    {
        $observerName .= ':'.get_class($model);
        self::$disabled[$observerName] = false;
    }

    /**
     * @param string $observerName
     * @param object $model
     */
    protected function disable($observerName, $model)
    {
        $observerName .= ':'.get_class($model);
        self::$disabled[$observerName] = true;
    }

    /**
     * @param string $observerName
     * @param object $model
     * @return bool
     */
    protected function active($observerName, $model)
    {
        $observerName .= ':'.get_class($model);
        return !array_key_exists($observerName, self::$disabled) || self::$disabled[$observerName] == false;
    }
} 