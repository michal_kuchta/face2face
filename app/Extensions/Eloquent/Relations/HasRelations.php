<?php namespace app\Extensions\Eloquent\Relations;

/**
 * Class HasRelations
 *
 * @package app\Extensions\Eloquent\Relations
 */
trait HasRelations
{
    /**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $foreignKey
     * @param  string  $keyValuesCallback
     * @return HasManyWithCallback
     */
    public function hasManyWithCallback($related, $foreignKey, $keyValuesCallback)
    {
        /** @var \Eloquent $instance */
        $instance = new $related;

        return new HasManyWithCallback($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $keyValuesCallback);
    }
} 