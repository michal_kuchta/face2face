<?php namespace app\Extensions\Eloquent\Relations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class HasManyWithCallback
 *
 * @package app\Extensions\Eloquent\Relations
 */
class HasManyWithCallback extends Relation
{

    /**
     * The foreign key of the parent model.
     *
     * @var string
     */
    protected $foreignKey;
    
    /**
     * The callback for getting key values of the parent model.
     *
     * @var \Closure
     */
    protected $keyValuesCallback;

    /**
     * Create a new has one or many relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string $foreignKey
     * @param  \Closure $keyValuesCallback
     */
    public function __construct(Builder $query, Model $parent, $foreignKey, $keyValuesCallback)
    {
        $this->foreignKey = $foreignKey;
        $this->keyValuesCallback = $keyValuesCallback;
        
        parent::__construct($query, $parent);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints)
        {
            $this->query->whereIn($this->foreignKey, $this->getKeyValues($this->parent));

            $this->query->whereNotNull($this->foreignKey);
        }
    }

    /**
     * Add the constraints for a relationship count query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationCountQuery(Builder $query, Builder $parent)
    {
        if ($parent->toBase()->from == $query->toBase()->from)
        {
            return $this->getRelationCountQueryForSelfRelation($query, $parent);
        }

        $query->select(new Expression('count(*)'));

        $keys = $this->getKeyValues($this->parent);

        return $query->whereIn($this->getHasCompareKey(), $keys);
    }

    /**
     * Add the constraints for a relationship count query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationCountQueryForSelfRelation(Builder $query, Builder $parent)
    {
        $query->select(new Expression('count(*)'));

        $tablePrefix = $this->query->toBase()->getConnection()->getTablePrefix();

        $query->from($query->getModel()->getTable().' as '.$tablePrefix.$hash = $this->getRelationCountHash());

        $keys = $this->getKeyValues($this->parent);

        return $query->whereIn($hash.'.'.$this->getPlainForeignKey(), $keys);
    }

    /**
     * Get a relationship join table hash.
     *
     * @return string
     */
    public function getRelationCountHash()
    {
        return 'self_'.md5(microtime(true));
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $this->query->whereIn($this->foreignKey, $this->getAllKeyValues($models));
    }

    /**
     * Get all of the keys for a model.
     *
     * @param  object $model
     * @return array
     */
    public function getKeyValues($model)
    {
        $keys = $model->{$this->keyValuesCallback}();
        
        return is_array($keys) ? $keys : [];
    }

    /**
     * Get all of the keys for an array of models.
     *
     * @param  array   $models
     * @return array
     */
    protected function getAllKeyValues(array $models)
    {
        $values = [];

        foreach ($models as $model)
        {
            $values = array_merge($values, $this->getKeyValues($model));
        }

        return array_unique(array_values($values));
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return $this->query->get();
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model)
        {
            $model->setRelation($relation, $this->related->newCollection());
        }

        return $models;
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array   $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        $dictionary = $this->buildDictionary($results);

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        foreach ($models as $model)
        {
            $keys = $this->getKeyValues($model);
            $values = [];
            
            foreach ($keys as $key)
            {
                if (isset($dictionary[$key]))
                {
                    $values[] = $dictionary[$key];
                }
            }

            $values = $this->related->newCollection($values);

            $model->setRelation($relation, $values);
        }

        return $models;
    }

    /**
     * Build model dictionary keyed by the relation's foreign key.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @return array
     */
    protected function buildDictionary(Collection $results)
    {
        $dictionary = array();

        $foreign = $this->getPlainForeignKey();

        // First we will create a dictionary of models keyed by the foreign key of the
        // relationship as this will allow us to quickly access all of the related
        // models without having to do nested looping which will be quite slow.
        foreach ($results as $result)
        {
            $dictionary[$result->{$foreign}] = $result;
        }

        return $dictionary;
    }

    /**
     * Get the foreign key for the relationship.
     *
     * @return string
     */
    public function getForeignKey()
    {
        return $this->foreignKey;
    }

    /**
     * Get the plain foreign key.
     *
     * @return string
     */
    public function getPlainForeignKey()
    {
        $segments = explode('.', $this->getForeignKey());

        return $segments[count($segments) - 1];
    }

    /**
     * Get the key for comparing against the parent key in "has" query.
     *
     * @return string
     */
    public function getHasCompareKey()
    {
        return $this->getForeignKey();
    }
}