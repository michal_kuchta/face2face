<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailController extends Mailable
{
    use Queueable, SerializesModels;

    private $mailViewName;
    private $mailViewData;
    
    
    /**
     * MailController::contruct()
     * 
     * @param string $viewName
     * @param array $data
     */
    public function __construct($viewName, $data = [])
    {
        if(!is_array($data))
        {
            $data = [$data];
        }
        
        $this->mailViewName = $viewName;
        $this->mailViewData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown($this->mailViewName, $this->mailViewData);
    }
}
