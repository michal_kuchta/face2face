<?php namespace App\Models;

use App\Models\Base\UserAbstract;
use App\Models\UserActivity;
use Illuminate\Support\Facades\Auth;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends UserAbstract
{
    public static function getMainPhotoUrl($user)
    {
        if(!empty($user->main_photo))
        {
            $image = url($user->main_photo);
        }
        else
        {
            switch($user->sex)
            {
                case 1:
                    $image = url('/images/female_placeholder.png');
                    break;
                default:
                    $image = url('/images/male_placeholder.png');
                    break;
            }
        }
        return $image;
    }
    
    public static function logUserActivity($action)
    {
        $activity = new UserActivity();
        $activity->user_id = Auth::user()->id;
        $activity->action = $action;
        $activity->created_at = \Carbon\Carbon::now();
        $activity->updated_at = \Carbon\Carbon::now();
        $activity->save();
    }
    
    public static function isUserActive($user)
    {
        $currentDateTime = \Carbon\Carbon::now();
        if($currentDateTime < date('Y-m-d H:i:s', (strtotime($user->last_activity) + (5*60))) && date('Y-m-d H:i:s', strtotime($user->last_signin)) > date('Y-m-d H:i:s', strtotime($user->last_logout)))
        {
            return true;
        }
        
        return false;
    }
}
