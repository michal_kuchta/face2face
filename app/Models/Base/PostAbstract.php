<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class PostAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\Post query()
 * @method static \App\Models\Post find($id)
 * @method static \App\Models\Post onlyTrashed()
 * @method static \App\Models\Post withTrashed()
 * @method static \App\Models\Post with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property integer user_id
 * @property string message
 * @property integer visibility
 * @property boolean allow_comments
 * @property boolean is_removed
 */
abstract class PostAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'id',
        'created_at',
        'updated_at',
        'user_id',
        'message',
        'visibility',
        'allow_comments',
        'is_removed'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'user_id',
        'message',
        'visibility',
        'allow_comments',
        'is_removed'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
