<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class PostimageAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\Postimage query()
 * @method static \App\Models\Postimage find($id)
 * @method static \App\Models\Postimage onlyTrashed()
 * @method static \App\Models\Postimage withTrashed()
 * @method static \App\Models\Postimage with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property integer post_id
 * @property string image_url
 * @property boolean is_removed
 * @property integer likes
 */
abstract class PostimageAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'postimages';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'id',
        'created_at',
        'updated_at',
        'post_id',
        'image_url',
        'is_removed',
        'likes'
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
