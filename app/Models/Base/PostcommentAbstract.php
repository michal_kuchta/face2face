<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class PostcommentAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\Postcomment query()
 * @method static \App\Models\Postcomment find($id)
 * @method static \App\Models\Postcomment onlyTrashed()
 * @method static \App\Models\Postcomment withTrashed()
 * @method static \App\Models\Postcomment with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property integer post_id
 * @property integer user_id
 * @property integer root_comment_id
 * @property integer visibility
 * @property string message
 * @property boolean is_removed
 * @property integer likes
 */
abstract class PostcommentAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'postcomments';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'id',
        'created_at',
        'updated_at',
        'post_id',
        'user_id',
        'root_comment_id',
        'visibility',
        'message',
        'is_removed',
        'likes'
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
