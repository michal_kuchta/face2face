<?php namespace App\Http\ViewModels\Post;

use App\Extensions\ViewModels\ViewModel;
use App\Models\Post;

/**
 * Class EditViewModel
 *
 * @package App\Http\ViewModels\Post\
 */
class EditViewModel extends ViewModel
{
    public $updated_at;
    public $message;
    public $visibility;
    public $allow_comments;
    
    /**
     * @param Content|array $data
     */
    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}