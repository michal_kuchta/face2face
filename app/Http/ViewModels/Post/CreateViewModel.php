<?php namespace App\Http\ViewModels\Post;

use App\Extensions\ViewModels\ViewModel;
use App\Models\Post;

/**
 * Class CreateViewModel
 *
 * @package App\Http\ViewModels\Post\
 */
class CreateViewModel extends ViewModel
{
    public $created_at;
    public $updated_at;
    public $user_id;
    public $message;
    public $visibility;
    public $allow_comments;
    public $is_removed;
    
    /**
     * @param Content|array $data
     */
    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
            'message' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}