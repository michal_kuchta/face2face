<?php namespace App\Http\ViewModels\Postcomment;

use App\Extensions\ViewModels\ViewModel;
use App\Models\Postcomment;

/**
 * Class CreateViewModel
 *
 * @package App\Http\ViewModels\Postcomment\
 */
class CreateViewModel extends ViewModel
{
    public $created_at;
    public $updated_at;
    public $post_id;
    public $user_id;
    public $root_comment_id;
    public $visibility;
    public $message;
    
    /**
     * @param Content|array $data
     */
    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
            
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}