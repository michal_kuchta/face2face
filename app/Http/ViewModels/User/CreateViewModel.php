<?php namespace App\Http\ViewModels\User;

use App\Extensions\ViewModels\ViewModel;
use App\Models\User;

/**
 * Class CreateViewModel
 *
 * @package App\Http\ViewModels\User\CreateViewModel
 */
class CreateViewModel extends ViewModel
{
    public $email;
    public $password;
    public $password2;
    public $userlogin;
    public $first_name;
    public $second_name;
    public $nick_name;
    public $activation_code;
    public $password_history;
    public $is_profile_public;
    public $is_activated;
    public $is_logged;
    public $last_signin;
    public $signup_date;
    public $last_password_change;
    public $last_account_change;
    public $about_me;
    public $date_of_birth;
    public $sex;
    public $main_photo;
    public $last_logout;
    
    /**
     * @param Content|array $data
     */
    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
            'email' => 'required|unique:users|email',
            'password' => ['required', 'min:6', 'max:20'],
            'password2' => ['required', 'min:6', 'max:20','same:password'],
            'userlogin' => 'unique:users',
            'first_name' => 'required',
            'second_name' => 'required',
            'date_of_birth' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}